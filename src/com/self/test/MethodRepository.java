package com.self.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class MethodRepository {
 private static final TimeUnit SECONDS = null;
WebDriver driver;
 public void googlelaunch() throws InterruptedException {
	 System.setProperty("webdriver.chrome.driver", "E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
	 driver= new ChromeDriver();
	 driver.get("https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=Us2DWqrsGPSG8Qf3t4PADQ");
	 Thread.sleep(3000);
		driver.manage().window().maximize();
		WebElement searchname = driver.findElement(By.xpath("//input[@dir='ltr']"));
		searchname.sendKeys("mercurytours");
		WebElement googlesearch = driver.findElement(By.xpath("//input[@value='Google Search']"));
		googlesearch.click();
		//driver.close();
 }

 public void mercurryappLaunch() {
	 WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	 driver.get("http://newtours.demoaut.com/");
	 WebElement uName = driver.findElement(By.name("userName"));
	 uName.sendKeys("dasd");
	 WebElement pWord = driver.findElement(By.name("password"));
	 pWord.sendKeys("dasd");
	 WebElement login = driver.findElement(By.xpath("//input[@alt='Sign-In']"));
	 login.click();
	 
			  
			    

	   
			  
 }
 public void flightbook() 
 {
 WebElement tripbutton = driver.findElement(By.xpath("//input[@value= 'oneway']"));
 tripbutton.sendKeys("One Way");
 driver.findElement(By.xpath("//input[@value= 'roundtrip']"));
 WebElement dropDown = driver.findElement(By.xpath("//select[@name= 'passCount']"));
 dropDown.sendKeys("2");
 WebElement dropDown1 = driver.findElement(By.xpath("//select[@name= 'fromPort']"));
 dropDown1.sendKeys("New York");
 WebElement dropDown2 = driver.findElement(By.xpath("//select[@name= 'fromMonth']"));
 dropDown2.sendKeys("February");
 WebElement dropDown3 = driver.findElement(By.xpath("//select[@name= 'fromDay']"));
 dropDown3.sendKeys("3");
 WebElement dropDown4 = driver.findElement(By.xpath("//select[@name= 'toPort']"));
 dropDown4.sendKeys("Paris");
 WebElement dropDown5 = driver.findElement(By.xpath("//select[@name= 'toMonth']"));
 dropDown5.sendKeys("March");
 WebElement dropDown6 = driver.findElement(By.xpath("//select[@name= 'toDay']"));
 dropDown6.sendKeys("15");
 }
 public void preferences() 
 {
 WebElement classbutton = driver.findElement(By.xpath("//input[@value= 'First']"));
 classbutton.sendKeys("First class");
 WebElement airprefer = driver.findElement(By.xpath("//select[@name='airline']"));
 airprefer.sendKeys("Unified Airlines");
 WebElement submit = driver.findElement(By.xpath("//input[@height= '23']"));
 submit.click();
 }
public void xcelMethod() throws IOException, BiffException 
{
	String FilePath = "D:\\data.xls";
	FileInputStream fs = new FileInputStream(FilePath);
	Workbook wb=Workbook.getWorkbook(fs);
	Sheet sh = wb.getSheet("Sheet1");

	// To get the number of rows present in sheet
	int totalNoOfRows = sh.getRows();
	// To get the number of columns present in sheet
	int totalNoOfCols = sh.getColumns();
	driver.get("http://newtours.demoaut.com/");
	for (int row = 1; row < totalNoOfRows; row++) {

		for (int col = 0; col < totalNoOfCols; col++) {
			System.out.print(sh.getCell(col, row).getContents() + "\t");
		}
		System.out.println("\n");
	}
}

public void map()
{
	WebDriver driver;
	System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.google.co.in/maps/@20.9857003,82.7525294,5z");
	WebElement input = driver.findElement(By.xpath("//input[@autocomplete='off']"));
	input.sendKeys("Gariahat, Kolkata, West Bengal");
	  input.sendKeys(Keys.ENTER);
		driver.close();

}
public void alertshow() throws InterruptedException
{
	WebDriver driver;
	System.setProperty("webdriver.chrome.driver", "E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://demo.guru99.com/test/delete_customer.php");
	WebElement submit = driver.findElement(By.xpath("//input[@ name='submit']"));
	submit.click();
	Alert alert = driver.switchTo().alert();
	 String alertMessage= driver.switchTo().alert().getText();
	 System.out.println(alertMessage);
	 Thread.sleep(3000);
	 alert.dismiss();
	 driver.close();
}
public void screenshot() throws Exception 
{
	WebDriver driver;
	System.setProperty("webdriver.chrome.driver", "E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://www.seleniumeasy.com/selenium-tutorials/take-screenshot-with-selenium-webdriver");
	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrFile, new File("c:\\temp\\screenshot.png"));
}
public void rightclick()
{

	WebDriver driver;
	System.setProperty("webdriver.chrome.driver", "E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://newtours.demoaut.com/");
	 WebElement uName = driver.findElement(By.name("userName"));
	Actions action= new Actions(driver);
	action.contextClick(uName).build().perform();

}
public void scroll()
{

	WebDriver driver;
	System.setProperty("webdriver.chrome.driver", "E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://newtours.demoaut.com/");
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("scroll(0,300);");

}



public void draganddrop()
{
	 WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("draggable")));
		Actions act=new Actions(driver);
		WebElement drag=driver.findElement(By.xpath(".//*[@id='draggable']"));
		WebElement drop=driver.findElement(By.xpath(".//*[@id='droppable']"));
		act.dragAndDrop(drag, drop).build().perform();
	
	
	
	
	
	
}
public void webhandling()
{

	 WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://money.rediff.com/gainers/bsc/daily/groupa?");
		 List <WebElement> col = driver.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
	        System.out.println("No of cols are : " +col.size()); 
	        List<WebElement>  rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]")); 
	        System.out.println("No of rows are : " + rows.size());
	        driver.close();
	
	
}
public void mercurryrobotappLaunch() throws AWTException {
	 WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	 driver.get("http://newtours.demoaut.com/");
	 WebElement uName = driver.findElement(By.name("userName"));
	 uName.sendKeys("dasd");
	 WebElement pWord = driver.findElement(By.name("password"));
	 pWord.sendKeys("dasd");
	 Robot app = new Robot();
	 app.keyPress(KeyEvent.VK_TAB);
	 app.keyRelease(KeyEvent.VK_TAB);
	 app.keyPress(KeyEvent.VK_ENTER);
	 app.keyRelease(KeyEvent.VK_ENTER);
	 
}
public static boolean romm() throws InterruptedException
{
	 WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\\\AUTOMATION TESTING\\\\TOOLS\\\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	 driver.get("http://www.benfishtourism.com/");
    WebElement startdate=  driver.findElement(By.id("startdate"));
    startdate.click();
	 
    WebElement checkin = driver.findElement(By.xpath("html/body/div[3]/div/div[2]/div/table/tbody/tr[1]/td[7]/a"));
    Thread.sleep(2500);
    checkin.sendKeys(Keys.ENTER);
    String str ="05/06/2018";
    System.out.println("05/06/2018");
	if (str.equalsIgnoreCase("05/06/2018")==true)
		return true;
	else 
		return false;
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}

}
